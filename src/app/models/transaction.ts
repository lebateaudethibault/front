export interface Transaction {
  tigID: number;
  price: number;
  quantity: number;
  type: string;
  date: Date;
}
