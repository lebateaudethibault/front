import { Injectable } from '@angular/core';
import {Transaction} from "../../models/transaction";
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class TransactionService {

  constructor(private http: HttpClient) { }

  getTransactions() {
    return this.http.get<Transaction[]>(environment.API_URL + "/transactions/");
  }

  addTransaction(tigID: number, price: number, quantity: number, type: string) {
    return this.http.get<Transaction>(environment.API_URL + "/transaction/" + tigID + "/" + type + "/" + price + "/" + quantity);
  }

  getTranstactionsByYearAndType(year: number, type: string) {
    return this.http.get<Transaction[]>(environment.API_URL + "/transaction/getannee/" + year + "/" + type);
  }
}
