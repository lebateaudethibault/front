import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Product} from "../../models/product";
import {Observable} from "rxjs";
import {environment} from "../../environments/environment";
import {Transaction} from "../../models/transaction";

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private http: HttpClient) { }

  getProductsFromJson(): Observable<Product[]> {
    return this.http.get<Product[]>(environment.API_URL + "/infoproducts/");
  }

  getProductsByCategory(category: string) {
    return this.http.get<Product[]>(environment.API_URL + "/infoproducts/" + category);
  }

  putNewPriceById(id: number, newPrice: number) {
    return this.http.get<Product>(environment.API_URL + "/putonsale/" + id + "/" + newPrice);
  }

  incrementStockById(id: number, quantity: number) {
    return this.http.get<Product>(environment.API_URL + "/incrementstock/" + id + "/" + quantity);
  }

  decrementStockById(id: number, quantity: number) {
    return this.http.get<Product>(environment.API_URL + "/decrementstock/" + id + "/" + quantity);
  }
}
