import {Component, OnInit} from '@angular/core';
import {ProductService} from "../../services/product/product.service";
import {Product} from "../../models/product";
import {MatDialog} from "@angular/material/dialog";
import {ModalProductComponent} from "../modal-product/modal-product.component";
import {Transaction} from "../../models/transaction";
import {TransactionService} from "../../services/transaction/transaction.service";

const COLUMNS_SCHEMA = [
  {
    key: "name",
    type: "text",
    label: "Name"
  },
  {
    key: "price",
    type: "number",
    label: "Price"
  },
  {
    key: "price_on_sale",
    type: "number",
    label: "Price on Sale"
  },
  {
    key: "discount",
    type: "number",
    label: "Discount"
  },
  {
    key: "quantityInStock",
    type: "number",
    label: "Quantity"
  },
  {
    key: "quantity_sold",
    type: "number",
    label: "Quantity Sold"
  },
  {
    key: "comments",
    type: "text",
    label: "Comments"
  }
];

interface Category {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})

export class ProductsComponent implements OnInit{
  columnsSchema: any = COLUMNS_SCHEMA;
  productList: Product[] = [];
  transactionList: Transaction[] = [];
  displayedColumns: string[] = COLUMNS_SCHEMA.map((col) => col.key);

  categoryFilter: string = "all";

  editingAll: boolean = false;

  categories: Category[] = [
    {value: "all", viewValue: "All"},
    {value: "poissons", viewValue: 'Poissons'},
    {value: "crustaces", viewValue: 'Crustacés'},
    {value: "coquillages", viewValue: 'Coquillages'},
  ];

  constructor(private productService: ProductService,
              public dialog: MatDialog,
              private transactionService: TransactionService) {
  }

  ngOnInit() {
    this.getAllList();
  }

  getAllList() {
    this.getTransactions();
    this.getProducts();
  }

  getTransactions(){
    this.transactionService.getTransactions().subscribe({
      next: (res: Transaction[]) => {
        this.transactionList = res.filter((transaction) => transaction.type === "vente");
      }
    })
  }

  getProducts() {
      this.productService.getProductsByCategory(this.categoryFilter).subscribe({
            next: (res: Product[]) => {
              this.productList = res;
              this.productList.map((product) => {
                const discountAmount = (product.price -(product.price * (product.discount / 100)) );
                const roundedDiscountAmount = discountAmount.toFixed(2);
                product.price_on_sale = parseFloat(roundedDiscountAmount);

                product.quantity_sold = this.transactionList
                    .filter((transaction) => transaction.tigID === product.tig_id)
                    .reduce((total, transaction) => total + transaction.quantity, 0);
              });
            },
            error: (e) => alert(e.message),
            complete: () => {}
          }
      );
  }

  openDialog(row: Product): void {
    const dialogRef = this.dialog.open(ModalProductComponent, {
      data: row,
    });

    dialogRef.afterClosed().subscribe(result => {
      this.getAllList();
    });
  }

  formatData(column: string, value: string) {
    switch (column) {
      case 'discount':
        return value + '%';

      case 'price':
      case 'price_on_sale':
        const val = parseFloat(value)
        return val.toFixed(2) + '€'

      default:
        return value;
    }
  }
}
