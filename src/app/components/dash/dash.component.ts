import {Component, OnInit} from '@angular/core';
import {TransactionService} from "../../services/transaction/transaction.service";
import {Transaction} from "../../models/transaction";
import { Chart, registerables } from 'node_modules/chart.js';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { FormControl } from '@angular/forms';
import { DatePipe } from '@angular/common'; 

Chart.register(...registerables)

@Component({
  selector: 'app-dash',
  templateUrl: './dash.component.html',
  styleUrls: ['./dash.component.css'],
  providers: [DatePipe],
})
export class DashComponent implements OnInit {
  date = new FormControl(new Date());
  private chart!: Chart;

  ctx : any;
  config : any;
  chartData : number[] = [];
  chartDatalabels : any[] = [];


  displayedColumns = ["tigID", "price", "quantity", "type"];
  transactionList: Transaction[] = [];


  annuelSales: number = 0;
  annuelBuy: number = 0;
  annuel: number = 0;

  impot: number = 0;


  constructor(private transactionService: TransactionService, private datePipe: DatePipe) {
  }

  ngAfterViewInit(): void {
    this.RenderChart();
    this.RenderPieChart();
  }

  ngOnInit() {
    this.transactionService.getTransactions().subscribe({
      next: (res) => {
        this.transactionList = res;
        this.refreshData();

      },
      error: (e) => alert(e)
    })
  }

  RenderPieChart(){
    this.chartData.push(1);
    this.chartData.push(2);
    this.chartData.push(3);


    this.ctx = document.getElementById('pieChart');
    this.config = {
      type : 'pie',
      options : {
      },
      data : {
        labels : this.chartDatalabels,
        datasets : [{ 
          label: 'Chart Data',
          data: this.chartData,
          borderWidth: 0,
          borderColor: 'grey',
          backgroundColor: ['pink', 'yellow','red']
      }],
      }
    }
    const myChart = new Chart(this.ctx, this.config);
  }
  RenderChart() {
    this.chart = new Chart('lineChart', {
      type: 'bar',
      data: {
        labels: ['Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi', 'Dimanche'],
        datasets: [
          {
            label: 'Ventes',
            data: [-5, 19, 3, 5, 2, 3, 5],
            borderWidth: 3,
          },
        ],
      },
      options: {
        scales: {
          y: {
            beginAtZero: true,
          },
        },
      },
    });
  }

  updateChart() {
    const categorySelector = document.getElementById('categoryFilter') as HTMLSelectElement;
    const selectedCategory = categorySelector.value;
    const data = this.getChartDataForCategory(selectedCategory);

    // Update the chart data and labels
    this.chart.data.labels = data.labels;
    this.chart.data.datasets[0].data = data.values;

    // Update the chart
    this.chart.update();
  }

  getChartDataForCategory(category: string) {
    // Define your data for different categories here
    switch (category) {
      case 'semainier':
        return { labels: ['Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi', 'Dimanche'], values: [12, 19, 3, 5, 2, 3, 6] };
      case 'mensuel':
        return { labels: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'], values: [15, 22, 7, 6, 9, 4, 15, 22, 7, 6, 9, 4] };
      case 'trimestriel':
        return { labels: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'], values: [8, 14, 10, 11, 5, 12, 14, 10, 11, 5, 12, 3] };
      case 'annuel':
        return { labels: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'], values: [30, 35, 18, 27, 42, 25, 30, 35, 18, 27, 42, 2] };
      default:
        return { labels: [], values: [] };
    }
  }

  formatDate(date: any, pattern: string): string {
    const formattedDate = this.datePipe.transform(date, pattern);
    return formattedDate ? formattedDate : '';
  }

  refreshData() {
    //annuel
    const customPattern = 'yyyy';
    const formattedDate = this.formatDate(this.date.value, customPattern);
    this.transactionService.getTranstactionsByYearAndType(parseInt(formattedDate), 'achat').subscribe({
      next: (res) => {
        const list = res;
        this.annuelBuy = list
        .reduce((total, transaction) => total + (transaction.quantity * transaction.price), 0);
      },
      error: (e) => alert(e)
    })
    this.transactionService.getTranstactionsByYearAndType(parseInt(formattedDate), 'vente').subscribe({
      next: (res) => {
        const list = res;
        this.annuelSales = list
        .reduce((total, transaction) => total + (transaction.quantity * transaction.price), 0);
      },
      error: (e) => alert(e),
      complete: () => {}
    })
    this.annuel = this.annuelSales - this.annuelBuy
    this.impot = parseFloat((this.annuel * 0.3).toFixed(2))
  }

}
