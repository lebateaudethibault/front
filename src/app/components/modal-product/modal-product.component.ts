import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {Product} from "../../models/product";
import {ProductService} from "../../services/product/product.service";
import {Transaction} from "../../models/transaction";
import {MatSnackBar} from "@angular/material/snack-bar";
import {TransactionService} from "../../services/transaction/transaction.service";

@Component({
  selector: 'app-modal-product',
  templateUrl: './modal-product.component.html',
  styleUrls: ['./modal-product.component.css']
})
export class ModalProductComponent {

  isLoading = false;
  newDiscount!: number;
  newQuantity!: number;
  newPrice!: number;

  constructor(public dialogRef: MatDialogRef<ModalProductComponent>,
              @Inject(MAT_DIALOG_DATA) public data: Product,
              private productService: ProductService,
              private transactionService: TransactionService,
              private _snackBar: MatSnackBar) {
    this.newDiscount = this.data.discount;
    this.newQuantity = this.data.quantityInStock;
    this.newPrice = this.data.price;
  }

  updateDiscount() {
    this.isLoading = true;
    if(this.newDiscount !== this.data.discount) {
      this.productService.putNewPriceById(this.data.id, this.newDiscount).subscribe({
          next: (response) => {
            this.isLoading = false;
            //this.dialogRef.close(response);
          },
          error: (e) => {
            alert(e);
          }
        }
      );
    }
    if(this.newQuantity !== this.data.quantityInStock) {
      if((this.newQuantity - this.data.quantityInStock) < 0) {
        const val = -(this.newQuantity - this.data.quantityInStock);
        this.productService.decrementStockById(this.data.id, val).subscribe({
            next: (response) => {
              this.isLoading = false;
              //this.dialogRef.close(response);
            },
            error: (e) => {
              alert(e);
            }
          }
        );
        // le cas où on vend un lot de produit
        if(this.newPrice !== 0){
          this.transactionService.addTransaction(this.data.tig_id, this.newPrice, val, "vente").subscribe({
            next: (response) => {
              this.isLoading = false;
              this.openSnackBar("Vous avez vendu " + response.quantity + " " + this.data.name + " pour " + response.price * response.quantity + '€');
              console.log("transaction = " + response);
              //this.dialogRef.close(response);
            },
            error: (e) => {
              alert(e);
            },
            complete: () => this.dialogRef.close()
          })
        } else {
          // le cas où on enlève un lot car produit périmé
          this.openSnackBar("Vous avez retiré " + val + " " + this.data.name + " périmé(e)s");
        }
      } else {
        console.log("yes");
        const val = (this.newQuantity - this.data.quantityInStock);
        this.productService.incrementStockById(this.data.id, val).subscribe({
            next: (response) => {
              this.isLoading = false;
              //this.dialogRef.close(response);
            },
            error: (e) => {
              alert(e);
            }
          }
        );
        // le cas où on refait le stock du produit (achat d'un lot de produit)
        this.transactionService.addTransaction(this.data.tig_id, this.newPrice, val, "achat").subscribe({
          next: (response) => {
            this.isLoading = false;
            this.openSnackBar("Vous avez acheté " + response.quantity + " " + this.data.name + " pour " + response.price * response.quantity + '€');
            console.log("transaction = " + response);
            //this.dialogRef.close(response);
          },
          error: (e) => {
            alert(e);
          },
          complete: () => this.dialogRef.close()
        })
      }

    }
    this.dialogRef.close();
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  openSnackBar(message: string) {
    this._snackBar.open(message, 'X', {
      horizontalPosition: 'center',
      verticalPosition: 'top',
    });
  }
}
