# Front
## Project setup

- to install the packages :

```
npm install
```

### Compiles and starts a server

- To starts a local development server :

```
ng serve
```
- or to open it automatically on the browser :
```
ng serve -o
```
-  `Then open your browser` : http://localhost:4200/
